import 'package:algolearn/screens/quiz/question.dart';
import 'package:algolearn/screens/quiz/quiz_brain.dart';
import 'package:flutter/material.dart';

import '../utils/const.dart';

class Category {
  final String title;
  final IconData icon;
  final Color color;
  final List<Question> quizQuestions;

  const Category(this.title, this.icon, this.color, this.quizQuestions);
}

class Article {
  final String title;
  final String content;
  final String? gifUrl;
  final String? videoUrl;

  const Article(this.title, this.content, this.gifUrl, this.videoUrl);
}

const Map<Category, List<Article>> data = {
  Category(
    'Знакомство с алгоритмами',
    Icons.star_sharp,
    Constants.blueShade50,
    algorithmsQuiz,
  ): [
    Article(
      'Введение',
      'assets/files/acquaintance/introduction.epub',
      'assets/files/acquaintance/introduction.gif',
      null,
    ),
    Article(
      'Что необходимо знать',
      'assets/files/acquaintance/need_to_know.epub',
      'assets/files/acquaintance/need_to_know.gif',
      null,
    ),
    Article(
      'Бинарный поиск',
      'assets/files/acquaintance/binary_search.epub',
      'assets/files/acquaintance/binary_search.gif',
      null,
    ),
    Article(
      'Более эффективный поиск',
      'assets/files/acquaintance/effective_method.epub',
      'assets/files/acquaintance/effective_method.gif',
      null,
    ),
    Article(
      'Время выполнения',
      'assets/files/acquaintance/executing_time.epub',
      'assets/files/acquaintance/executing_time.gif',
      null,
    ),
  ],
  Category(
    'Сортировка выбором',
    Icons.star_sharp,
    Constants.blueAccent,
    selectionSortQuiz,
  ): [
    Article(
      'Как работает память!',
      'assets/files/sort/how_memory_works.epub',
      'assets/files/sort/how_memory_works.gif',
      null,
    ),
    Article(
      'Массивы',
      'assets/files/sort/arrays.epub',
      'assets/files/sort/arrays.gif',
      null,
    ),
    Article(
      'Связанные списки',
      'assets/files/sort/linked_list.epub',
      'assets/files/sort/linked_list.gif',
      null,
    ),
    Article(
      'Сортировка выбором',
      'assets/files/sort/sort.epub',
      'assets/files/sort/sort.gif',
      null,
    ),
  ],
  Category(
    'Рекурсия',
    Icons.star_sharp,
    Constants.blueShade50,
    recursionQuiz,
  ): [
    Article(
      'Рекурсия',
      'assets/files/recursion/recursion.epub',
      'assets/files/recursion/recursion.gif',
      null,
    ),
    Article(
      'Базовый случай и рекурсивныи случаи',
      'assets/files/recursion/recursion_2.epub',
      'assets/files/recursion/recursion.gif',
      null,
    ),
    Article(
      'Стек',
      'assets/files/recursion/stack.epub',
      'assets/files/recursion/stack.gif',
      null,
    ),
  ],
  Category(
    'Разные способы сортировки',
    Icons.star_sharp,
    Constants.blueAccent,
    otherSortQuiz,
  ): [
    Article(
      'Сортировка пузырьком',
      'assets/files/sortings/Сортировка-пузырьком.epub',
      'assets/files/sortings/bubble_1.gif',
      null,
    ),
    Article(
      'Сортировка перемешиванием (шейкерная сортировка)',
      'assets/files/sortings/Сортировка-перемешиванием.epub',
      'assets/files/sortings/shaker_1.gif',
      null,
    ),
    Article(
      'Сортировка расчёской',
      'assets/files/sortings/Сортировка-расчёской.epub',
      'assets/files/sortings/расчёска.gif',
      null,
    ),
    Article(
      'Сортировка вставками',
      'assets/files/sortings/Сортировка-вставками.epub',
      'assets/files/sortings/сорт_вставками.gif',
      null,
    ),
    Article(
      'Сортировка выбором',
      'assets/files/sortings/Сортировка-перемешиванием.epub',
      'assets/files/sortings/сорт_выбором.gif',
      null,
    ),
    Article(
      'Быстрая сортировка',
      'assets/files/sortings/Сортировка-перемешиванием.epub',
      'assets/files/sortings/быстрая_сорт.gif',
      null,
    ),
    Article(
      'Пирамидальная сортировка',
      'assets/files/sortings/Сортировка-перемешиванием.epub',
      'assets/files/sortings/пирамид_сортировка.gif',
      null,
    ),
  ],
};
