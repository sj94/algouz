import 'package:algolearn/data/data.dart';
import 'package:algolearn/screens/quiz/question.dart';
import 'package:algolearn/screens/quiz/quiz_brain.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import '../../utils/const.dart';

class QuizPage extends StatefulWidget {
  const QuizPage({Key? key, required this.category}) : super(key: key);

  final Category category;

  @override
  State createState() => _QuizPageState();
}

class _QuizPageState extends State<QuizPage> {
  List<Icon> scoreKeeper = [];
  late final quizBrain = QuizBrain(widget.category.quizQuestions);

  static const variantPrefixes = ['А', 'Б', 'В', 'Г', 'Д'];

  void checkAnswer(Answer userPickedAnswer) {
    bool correctAnswer = userPickedAnswer.correct;

    setState(() {
      if (quizBrain.isFinished()) {
        // quizBrain.reset();
        // scoreKeeper = [];
      } else {
        if (correctAnswer) {
          scoreKeeper.add(const Icon(
            Icons.check,
            color: Colors.greenAccent,
          ));
        } else {
          scoreKeeper.add(const Icon(
            Icons.close,
            color: Colors.redAccent,
          ));
        }
        quizBrain.nextQuestion();
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final question = quizBrain.getQuestion();
    final answers = question?.answers.toList(growable: false);

    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        leading: TextButton(
          style: TextButton.styleFrom(
            padding: const EdgeInsets.all(0),
            primary: Colors.white.withOpacity(0.3),
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10.0),
            ),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
          child: const Icon(Icons.keyboard_backspace, color: Colors.white),
        ),
        title: Text(
          "Тесты - (${widget.category.title})",
          maxLines: 1,
          style: const TextStyle(
            fontSize: 20,
            fontWeight: FontWeight.w900,
            color: Colors.white,
          ),
        ),
      ),
      body: Column(
        children: <Widget>[
          if (question == null || answers == null)
            ..._onFinishedWidget()
          else
            ..._onQuizStarted(question, answers),
/*          Row(
            children: <Widget>[
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: Constants.blueAccent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                    textStyle: GoogleFonts.amaticSc(fontSize: 30)),
                child: SizedBox(
                  height: 75,
                  width: MediaQuery.of(context).size.width * 0.38,
                  child: const Text(
                    "А",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 25.0,
                        fontWeight: FontWeight.w900,
                        color: Colors.black),
                  ),
                ),
                onPressed: () {
                  checkAnswer(true);
                },
              ),
              ElevatedButton(
                style: ElevatedButton.styleFrom(
                    primary: Constants.blueAccent,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(18.0),
                    ),
                    textStyle: GoogleFonts.amaticSc(fontSize: 30)),
                child: SizedBox(
                  height: 75,
                  width: MediaQuery.of(context).size.width * 0.38,
                  child: const Text(
                    "Б",
                    textAlign: TextAlign.center,
                    style: TextStyle(
                        fontSize: 25.0,
                        fontWeight: FontWeight.w900,
                        color: Colors.black),
                  ),
                ),
                onPressed: () {
                  checkAnswer(false);
                },
              )
            ],
          )*/
          SizedBox(
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
              child: Row(
                children: [
                  ...scoreKeeper,
                  const Spacer(),
                  Text(
                    'Вопрос ${quizBrain.displayQuestionNumber()} из ${quizBrain.questions.length}',
                    style: const TextStyle(
                      fontSize: 25,
                      fontWeight: FontWeight.bold,
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  List<Widget> _onQuizStarted(Question question, List<Answer> answers) {
    return [
      Expanded(
        flex: 5,
        child: Padding(
          padding: const EdgeInsets.all(16.0),
          child: Center(
            child: Text(
              quizBrain.getQuestionText(),
              style: GoogleFonts.ubuntuMono(
                fontSize: 20.0,
                color: Colors.black,
              ),
            ),
          ),
        ),
      ),
      ListView.separated(
          padding: const EdgeInsets.all(12),
          shrinkWrap: true,
          physics: const NeverScrollableScrollPhysics(),
          itemBuilder: (context, index) {
            return ElevatedButton(
              style: ElevatedButton.styleFrom(
                primary: Constants.blueAccent,
                shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(18.0),
                ),
                textStyle: GoogleFonts.amaticSc(
                  fontSize: 30,
                ),
                alignment: Alignment.centerLeft,
              ),
              child: Padding(
                padding: const EdgeInsets.all(16.0),
                child: Text(
                  "(${variantPrefixes[index]}.)  ${answers[index].text}",
                  style: const TextStyle(
                    fontSize: 25.0,
                    fontWeight: FontWeight.w900,
                    color: Colors.black,
                  ),
                ),
              ),
              onPressed: () {
                checkAnswer(answers[index]);
              },
            );
          },
          separatorBuilder: (context, index) {
            return const SizedBox(
              height: 12,
            );
          },
          itemCount: question.answers.length),
    ];
  }

  List<Widget> _onFinishedWidget() {
    return [
      const Spacer(),
      Center(
        child: ElevatedButton(
            onPressed: () {
              setState(() {
                quizBrain.reset();
                scoreKeeper = [];
              });
            },
            style: ElevatedButton.styleFrom(
                textStyle: GoogleFonts.amaticSc(
                  fontSize: 30,
                ),
                alignment: Alignment.centerLeft,
                padding: const EdgeInsets.all(16)),
            child: const Text('пробовать снова')),
      ),
      const SizedBox(height: 10),
      Center(
        child: Text(
          'твой результат ${_getCorrectAnswerCount()} из ${quizBrain.questions.length}',
          style: TextStyle(fontSize: 35, fontWeight: FontWeight.bold),
        ),
      ),
      const Spacer(),
    ];
  }

  int _getCorrectAnswerCount() {
    return scoreKeeper.where((element) => element.icon == Icons.check).length;
  }
}
