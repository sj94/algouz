class Question {
  final String questionText;
  final Set<Answer> answers;

  const Question(this.questionText, this.answers);
}

class Answer {
  final String text;
  final bool correct;

  const Answer(this.text, this.correct);
}
