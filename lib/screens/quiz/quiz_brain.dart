import 'question.dart';

class QuizBrain {
  int _questionNumber = 0;

  int displayQuestionNumber() {
    var displayNumber = _questionNumber + 1;
    if (displayNumber <= questions.length) {
      return displayNumber;
    } else {
      return questions.length;
    }
  }

  final List<Question> questions;

  QuizBrain(this.questions);

  void nextQuestion() {
    if (_questionNumber < questions.length) {
      _questionNumber++;
    }
  }

  Question? getQuestion() {
    try {
      return questions[_questionNumber];
    } catch (_) {
      return null;
    }
  }

  String getQuestionText() {
    return questions[_questionNumber].questionText;
  }

  bool isFinished() {
    if (_questionNumber >= questions.length) {
      return true;
    } else {
      return false;
    }
  }

  void reset() {
    _questionNumber = 0;
  }
}

const List<Question> algorithmsQuiz = [
  Question(
    'Какие существуют метрики, отображающие эффективность алгоритма?',
    {
      Answer('процессорное время, память', true),
      Answer('надежность, масштабируемость', false),
      Answer('адаптивность, простота реализации', false),
      Answer('память', false),
    },
  ),
  Question(
    'В функциональной парадигме при проектировании алгоритма, какой оценкой на время работы интересуются?',
    {
      Answer('оценкой в худшем случае', true),
      Answer('оценкой в среднем', false),
      Answer('оценкой в лучшем случае', false),
      Answer('оценкой в наивысшем случае', false),
    },
  ),
  Question(
    'При размере входных данных N, как рассчитывается время работы алгоритма?',
    {
      Answer('не зависимо от N', false),
      Answer('в сравнении с N', false),
      Answer('как функция от параметра N', true),
      Answer('зависимо от N', false),
    },
  ),
  Question(
    'Чтобы алгоритм бинарного поиска работал правильно, нужно, чтобы массив (список) был:',
    {
      Answer('Отсортированным', true),
      Answer('Несортированным', false),
      Answer('В куче', false),
      Answer('Выходящим из стека', false),
    },
  ),
];

const List<Question> selectionSortQuiz = [
  Question(
    'Определите максимальное количество узлов в двоичном дереве с высотой k, где корень — нулевая высота (0).',
    {
      Answer('2ᵏ − 1', true),
      Answer('2ᵏ⁺¹ – 1', false),
      Answer('2ᵏ⁻¹ + 1', false),
      Answer('2ᵏ + 1', false),
    },
  ),
  Question(
    'Что означает следующая фраза: «алгоритм X асимптотически более эффективен, чем Y»?',
    {
      Answer('X будет лучшим выбором для всех входов', false),
      Answer(
          'X будет лучшим выбором для всех входов, за исключением, возможно, небольших входов',
          true),
      Answer('X будет лучшим выбором для всех входов, кроме больших входов',
          false),
      Answer('Y будет лучшим выбором для небольших входов', false),
    },
  ),
  Question(
    'Алгоритм обхода графа отличается от алгоритма обхода вершин дерева тем, что…',
    {
      Answer('Деревья не соединяются', false),
      Answer('Графы могут иметь циклы', true),
      Answer('У деревьев есть корни', false),
      Answer(
          'Все утверждения выше ошибочны: дерево — подмножество графа', false),
    },
  ),
  Question(
    'Какой алгоритм из нижеперечисленных будет самым производительным, если дан уже отсортированный массив?',
    {
      Answer('Сортировка слиянием', false),
      Answer('Сортировка вставками', true),
      Answer('Быстрая сортировка', false),
      Answer('Пирамидальная сортировка', false),
    },
  ),
];

const List<Question> recursionQuiz = [
  Question(
    'Что делает следующая функция на языке C?\n\n'
    'int trial(int a, int b, int c) {\n'
    '\t\tif ((a >= b) && (c < b))\n'
    '\t\t\t\treturn b;\n'
    '\t\telse if (a>=b)\n'
    '\t\t\t\treturn Trial(a, c, b);\n'
    '\t\telse\n'
    '\t\t\t\treturn Trial(b, a, c);\n'
    '}',
    {
      Answer('Находит максимальное значение a, b и c', false),
      Answer('Находит минимальное значение a, b и c', false),
      Answer('Находит среднее число a, b и c', false),
      Answer('Ничего из вышеперечисленного', true),
    },
  ),
  Question(
    'Алгоритм Дейкстры основан на:',
    {
      Answer('Парадигме «разделяй и властвуй»', false),
      Answer('Динамическом программировании', false),
      Answer('Жадном подходе (Greedy Approach)', true),
      Answer('Поиске с возвратом', false),
    },
  ),
  Question(
    'Какой алгоритм не основан на жадном подходе?',
    {
      Answer('Алгоритм нахождения кратчайшего пути Дейкстры', false),
      Answer('Алгоритм Прима', false),
      Answer('Алгоритм Крускала', false),
      Answer('Алгоритм нахождения кратчайшего пути Беллмана-Форда', true),
    },
  ),
  Question(
    'Рассмотрите программу ниже и определите её сложность.\n'
    'void function(int n) {\n\n'
    '\t\tint i, j, count=0;\n'
    '\t\tfor (i=n/2; i <= n; i++)\n'
    '\t\t\t\tfor (j = 1; j <= n; j = j*2)\n'
    '\t\t\t\t\t\tcount++;\n'
    '}',
    {
      Answer('O(log n)', false),
      Answer('O(n²)', false),
      Answer('O(n² log n)', false),
      Answer('O(n log n)', true),
    },
  ),
];

const List<Question> otherSortQuiz = [
  Question(
    'При рассмотрении времени работы T(M) и памяти M(N) что нас интересует?',
    {
      Answer('точный вид функций T(N) и M(N)', false),
      Answer('приближенный до константы вид функций. Используется O-символика',
          true),
      Answer('приближенный вид функций. Используется o-символика', false),
      Answer('T(N)', false),
    },
  ),
  Question(
    'При оценивании функций какая оценка соответствует символике f = O(g)?',
    {
      Answer('оценка снизу', false),
      Answer('оценка сверху', true),
      Answer('асимптотическое равенство ', false),
      Answer('равенство', false),
    },
  ),
  Question(
    'При оценивании функций символике f = Θ(g) соответствует:',
    {
      Answer('оценка снизу', false),
      Answer('оценка сверху', false),
      Answer('асимптотическое равенство', true),
      Answer('равенство', false),
    },
  ),
  Question(
    'Если при оценивании фиксированного алгоритма оценки сверху и снизу совпали, то какие действия предпринимаются?',
    {
      Answer('оценка снизу', true),
      Answer('оценка сверху', false),
      Answer('асимптотическое равенство', false),
      Answer('без оценки', false),
    },
  ),
];
