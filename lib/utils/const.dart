import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class Constants {
  // Name
  static String appName = "e-Learning";

  //Category color
  static const Color blueAccent = Color(0xFF80D8FF);
  static const Color blueShade50 = Color(0xFFE3F2FD);

  // Material Design Color
  static const Color lightPrimary = Color(0xfffcfcff);
  static const Color lightAccent = Color(0xFFF18C8E);
  static const Color lightBackground = Color(0xfffcfcff);

  static const Color grey = Color(0xff707070);
  static const Color textPrimary = Color(0xFF486581);
  static const Color textDark = Color(0xFF305F72);

  // Salmon
  static const Color salmonMain = Color(0xFFF18C8E);
  static const Color salmonDark = Color(0xFFBB7F87);
  static const Color salmonLight = Color(0xFFF19895);

  // Blue

  static const Color blueMain = Color(0xFF579ACA);
  static const Color blueDark = Color(0xFF4E92B1);
  static const Color blueLight = Color(0xFF5AA6C8);

  // Pink
  static const Color lightPink = Color(0xFFFAE4F4);

  // Yellow
  static const Color lightYellow = Color(0xFFFFF5E5);

  // Violet
  static const Color lightViolet = Color(0xFFFBF5FF);

  static ThemeData lighTheme(BuildContext context) {
    return ThemeData(
      backgroundColor: lightBackground,
      primaryColor: lightPrimary,
      accentColor: lightAccent,
      cursorColor: lightAccent,
      textTheme: TextTheme(
          bodyText1: GoogleFonts.amaticSc(), bodyText2: GoogleFonts.amaticSc()),
      scaffoldBackgroundColor: lightBackground,
      appBarTheme: AppBarTheme(
        titleTextStyle: GoogleFonts.amaticSc(),
        iconTheme: const IconThemeData(
          color: lightAccent,
        ),
      ),
    );
  }

  static const double headerHeight = 228.5;
  static const double mainPadding = 20.0;
}
